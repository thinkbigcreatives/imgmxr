<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Stylesheet -->
        <link rel="stylesheet" href="{{asset('css/canvas.css')}}">

        <!-- Javascripts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="{{asset('js/canvas.js')}}"></script>
</head>
<body>
    <main id="app">
        <img id="logo" src="{{asset('img/golive.png')}}">
		<div id="canvas"></div>
		<div id="previews"></div>
        <div id="timer"></div>
	</main>

    <form id="completed" method="POST" action="{{route('users.update',$user->id)}}">
        @method('patch')
        @csrf
        <input id="finalTimer" type="hidden" name="timer" value="">
    </form>
</body>
</html>
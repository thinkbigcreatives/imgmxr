<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.2.15/tailwind.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="{{asset('js/imgmxr.js')}}"></script>
    </head>
    <body class="w-full h-full bg-fixed bg-cover" style="background-image: url({{asset('img/bg.jpg')}})" onload="imgMxr()">
        <div id="timer" class="absolute top-0 right-0 z-50 mt-12 mr-12 text-3xl font-extrabold text-gray-900">
            0
        </div>
        <div class="flex items-center justify-center w-screen h-screen">
            <div id="game" class="grid grid-cols-2 gap-2 md:grid-cols-4">
            </div>
        </div>
        <form id="completed" method="POST" action="{{route('users.update',$user->id)}}">
            @method('patch')
            @csrf
            <input id="finalTimer" type="hidden" name="timer" value="">
        </form>
    </body>
</html>
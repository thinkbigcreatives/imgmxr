<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <p class="font-extrabold text-gray-900 text-md">INSTRUCTIONS</p>

        <ol class="py-2 text-sm text-justify text-gray-700 list-decimal list-inside">
            <li class="">Register your name and email address</li>
            <li class="">Press the "Play Now" button to start the game</li>
            <li class="">There will be six pairs of identical cards</li>
            <li class="">To win the game, you must identify six matching pairs</li>
            <li class="">Good luck!</li>
        </ol>

        <x-jet-validation-errors class="mb-4" />

        <hr class="py-2">

        <form method="POST" action="{{ route('users.store') }}">
            @csrf

            <div>
                <x-jet-label for="name" value="{{ __('Fullname') }}" />
                <x-jet-input id="name" class="block w-full mt-1" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email Address') }}" />
                <x-jet-input id="email" class="block w-full mt-1" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="flex flex-col items-center justify-end mt-4">
                <x-jet-button class="w-full text-center">
                    {{ __('PLAY NOW') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>

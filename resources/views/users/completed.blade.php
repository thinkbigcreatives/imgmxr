<x-guest-layout>
    <div class="flex flex-col items-center justify-center w-screen h-screen p-20 border-4 border-blue-300 bg-white/50">
        <h1 class="text-2xl font-extrabold text-left">Player No. {{$player->id}} - {{$player->name}} ({{$player->email}}),</h1>
        <p class="py-6 text-xl font-bold text-justify">
            CONGRATULATIONS for completing the Memory Game in under {{$player->timer}} seconds and being part of the top participants. <br> <span class="text-left">- AMI Team</span>
        </p>
    </div>
</x-guest-layout>
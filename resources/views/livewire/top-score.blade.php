<div>
    <ul class="space-y-6 list-disc text-md">
        @foreach($users as $user)
        <li class="flex flex-col">
            <p class="text-sm text-yellow-500 uppercase text-extrabold">
                {{$user->name}} <span class="text-gray-900">({{$user->email}})</span>
            </p>
            <small class="text-xs">{{$user->timer}} seconds, {{$user->created_at}}</small>
        </li>
        @endforeach
    </ul>
</div>

var images, firstPick = null, secondPick = null, countPick = 0, score = 0;

var cards = [
    {
        name: "AMI Group",
        img: "../img/blue.png",
        id: 1
    },
    {
        name: "Dynamic",
        img: "../img/green.png",
        id: 2
    },
    {
        name: "Origami",
        img: "../img/red.png",
        id: 3
    },
    {
        name: "Circle",
        img: "../img/circle.png",
        id: 4
    },
    {
        name: "Triangle",
        img: "../img/triangle.png",
        id: 5
    },
    {
        name: "Square",
        img: "../img/square.png",
        id: 6
    },
];

function toggleElem(paramID)
{
	document.getElementById(paramID).classList.toggle('hidden');
	document.getElementById(paramID).classList.toggle('block');
}

function shuffleMxr(array){
    var counter = array.length, temp, index;
    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        index = Math.floor(Math.random() * counter);
        // Decrease counter by 1
        counter--;
        // And swap the last element with it
        temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }
    return array;
}

function imgMxr() {

    var game = document.getElementById('game');
    var cardList = $.merge(this.cards,this.cards);
    this.images = this.shuffleMxr(cardList);
    
    Object.keys(this.images).forEach(function(key) {
        var cardbtn = document.createElement("button");
        cardbtn.setAttribute("id", 'card'+key);
        cardbtn.setAttribute("data-id", this.images[key].id);
        cardbtn.classList.add('h-24', 'w-24', 'md:h-44', 'md:w-44', 'bg-white', 'p-2', 'md:p-6', 'border', 'border-gray-900', 'rounded-xl');

        var cardback = document.createElement("img");
        cardback.setAttribute("id", 'back'+key);
        cardback.setAttribute("src", '../img/card.png');
        
        var cardfront = document.createElement("img");
        cardfront.setAttribute("id", 'front'+key);
        cardfront.setAttribute("src", this.images[key].img);
        cardfront.classList.add('hidden');

        cardbtn.onclick = function() {
            checkGame(key);
		}
        
        cardbtn.appendChild(cardback);
        cardbtn.appendChild(cardfront);

        game.appendChild(cardbtn);
    });
}

function checkGame(key) {
    
    toggleElem('back'+key);
    toggleElem('front'+key);

    if(this.countPick == 0) {
        this.firstPick = key;
        countPick++;
    } else {
        this.secondPick = key;
        countPick++;
    }

    if(this.firstPick != null && this.secondPick != null && this.countPick == 2) {
        if(this.images[this.firstPick].id == this.images[this.secondPick].id) {
            this.score++;
            setTimeout(function(){ 
                toggleElem('card'+this.firstPick);
                toggleElem('card'+this.secondPick);
                this.countPick = 0;
            }, 1000);
        } else {
            setTimeout(function(){ 
                toggleElem('back'+this.firstPick);
                toggleElem('front'+this.firstPick);
                toggleElem('back'+this.secondPick);
                toggleElem('front'+this.secondPick);
                this.countPick = 0;
            }, 1000);
        }
    }

    if(this.firstPick != null && this.secondPick != null  && this.countPick == 0) {
        this.firstPick = null;
        this.secondPick = null;
    }

    if(this.score == 6)
    {
        completedPuzzle();
    }
}

var sec = 0;
var myClock = setInterval(myTimer, 1000);

function myTimer() {
    document.getElementById('timer').innerHTML = ++sec;
}

function completedPuzzle() {
    document.getElementById('finalTimer').value = sec;
    clearInterval(myClock);
    document.getElementById('completed').submit();
}
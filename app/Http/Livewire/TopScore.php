<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class TopScore extends Component
{
    public $users;

    public function mount(User $users)
    {
        $this->users = $users->where('admin',false)->orderBy('completed','asc')->orderBy('created_at')->get();
    }

    public function render()
    {
        return view('livewire.top-score');
    }
}
